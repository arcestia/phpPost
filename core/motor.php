<?php
include('config.php');
include('Parsedown.php');
define('VERSION', '0.3');

class motor{
    private $action;
    private $posts;
    private $comments;
    private $logged;
    private $token;
    
    public function __construct(){
        error_reporting(E_ALL);
        session_start();
        $this->logged = (isset($_SESSION['admin'])) ? true : false;
        $this->token = (isset($_SESSION['token'])) ? $_SESSION['token'] : '';
        if(!file_exists('data')) $this->action = 'install';
        elseif(isset($_POST['del']) && $this->isAutorized()) $this->action = 'delPost';
        elseif(isset($_POST['savepost']) && $this->isAutorized()) $this->action = 'savePost';
        elseif(isset($_POST['login'])) $this->action = 'login';
        elseif(isset($_POST['comment'])) $this->action = 'addComment';
        elseif(isset($_GET['post'])) $this->action = 'readPost';
        elseif(isset($_GET['edit']) && $this->isAutorized()) $this->action = 'editPost';
        elseif(isset($_GET['del']) && $this->isAutorized()) $this->action = 'delComment';
        elseif(isset($_GET['logout']) && $this->isAutorized()) $this->action = 'logout';
        else $this->action = 'listPosts';
        $this->posts = $this->readPosts();
        $this->comments = array();
    }
    
    public function get($k){
        return $this->$k;
    }
    
    public function install(){
        mkdir('data');
        mkdir('data/post');
        mkdir('data/upload');
        mkdir('data/comment');
        $data = "PCEtLQ0KTXkgZmlyc3QgcG9zdA0KMjAxNS0wOS0yOSAxMzoxNToyNQ0KLS0+DQpTZWQgbWF4aW11bSBlc3QgaW4gYW1pY2l0aWEgcGFyZW0gZXNzZSBpbmZlcmlvcmkuIFNhZXBlIGVuaW0gZXhjZWxsZW50aWFlIHF1YWVkYW0gc3VudCwgcXVhbGlzIGVyYXQgU2NpcGlvbmlzIGluIG5vc3RybywgdXQgaXRhIGRpY2FtLCBncmVnZS4gTnVtcXVhbSBzZSBpbGxlIFBoaWxvLCBudW1xdWFtIFJ1cGlsaW8sIG51bXF1YW0gTXVtbWlvIGFudGVwb3N1aXQsIG51bXF1YW0gaW5mZXJpb3JpcyBvcmRpbmlzIGFtaWNpcywgUS4gdmVybyBNYXhpbXVtIGZyYXRyZW0sIGVncmVnaXVtIHZpcnVtIG9tbmlubywgc2liaSBuZXF1YXF1YW0gcGFyZW0sIHF1b2QgaXMgYW50ZWliYXQgYWV0YXRlLCB0YW1xdWFtIHN1cGVyaW9yZW0gY29sZWJhdCBzdW9zcXVlIG9tbmVzIHBlciBzZSBwb3NzZSBlc3NlIGFtcGxpb3JlcyB2b2xlYmF0Lg==";
        if(file_put_contents('data/post/my-first-post.txt', base64_decode($data))){
            $data = "deny from all";
            $data2 = "allow from all";
            if(file_put_contents('data/.htaccess', $data) && file_put_contents('data/upload/.htaccess', $data2)){
                return true;
            }
        }
        return false;
    }
    
    public function searchPost($id){
        foreach($this->posts as $k=>$obj){
            if($obj->get('id') == $id) return $this->posts[$k];
        }
        return false;
    }
    
    public function readComments($parent){
        $data = array();
        if(file_exists('data/comment/'.$parent)){
            foreach(scandir('data/comment/'.$parent) as $fileName) if($fileName[0] != '.'){
                $content = file_get_contents('data/comment/'.$parent.'/'.$fileName);
                $id = str_replace('.txt', '', $fileName);
                preg_match('/<!--(.*?)-->/Uis', $content, $temp);
                $temp = explode("\n", $temp[1]);
                $post = new comment($id,  $temp[1], $temp[2], $content);
                $data[] = $post;
            }
        }
        return $data;
    }
    
    public function countComments($id){
        return count($this->readComments($id));
    }
    
    public function countPosts(){
        return count($this->posts);
    }
    
    public function saveComment($comment){
        if(!file_exists('data/comment/'.$comment->get('parent'))){
            mkdir('data/comment/'.$comment->get('parent'));
        }
        $comment->set('id', time());
        $comment->set('date', date('Y-m-d H:i:s'));
        $data = "<!--\n";
        $data.= $comment->get('author')."\n";
        $data.= $comment->get('date')."\n";
        $data.= "-->\n";
        $data.= $comment->get('content');
        return file_put_contents('data/comment/'.$comment->get('parent').'/'.$comment->get('id').'.txt', $data);
    }
    
    public function savePost($post){
        if($post->get('id') == '') $post->set('id', $this->idPost($post->get('title')));
        $data = "<!--\n";
        $data.= $post->get('title')."\n";
        $data.= $post->get('date')."\n";
        $data.= "-->\n";
        $data.= $post->get('content');
        return file_put_contents('data/post/'.$post->get('id').'.txt', $data);
    }
    
    public function login($password){
        if(PASSWORD == sha1($password)){
            $_SESSION['admin'] = uniqid();
            $_SESSION['token'] = uniqid();
            return true;
        }
        else{
            unset($_SESSION['admin']);
            unset($_SESSION['token']);
            return false;
        }
    }
    
    public function logout(){
        unset($_SESSION['admin']);
        unset($_SESSION['token']);
    }
    
    public function delComment($parent, $id){
        return unlink('data/comment/'.$parent.'/'.$id.'.txt');
    }
    
    public function delPost($id){
        return unlink('data/post/'.$id.'.txt');
    }
    
    public function isAutorized(){
        if($this->logged && isset($_REQUEST['token']) && $_REQUEST['token'] == $this->token) return true;
        return false;
    }
    
    public function checkCaptcha($str, $search, $pos){
        if($str[$pos] == $search) return true;
        return false;
    }
    
    private function readPosts(){
        $data = array();
        if(file_exists('data/post')){
            foreach(scandir('data/post') as $fileName) if($fileName[0] != '.'){
                $id = str_replace('.txt', '', $fileName);
                $content = file_get_contents('data/post/'.$fileName);
                preg_match('/<!--(.*?)-->/Uis', $content, $temp);
                $temp = explode("\n", $temp[1]);
                $post = new post($id, $temp[1], $temp[2], $content);
                $data[] = $post;
            }
            $temp = array();
            foreach($data as $k=>$obj){
                $temp[$k] = $obj->get('date');
            }
            array_multisort($temp, SORT_DESC, $data);
        }
        return $data;
    }
    
    private function idPost($title){
    	$title = str_replace('&', 'et', $title);
    	if($title !== mb_convert_encoding(mb_convert_encoding($title,'UTF-32','UTF-8'),'UTF-8','UTF-32')) $title = mb_convert_encoding($title,'UTF-8');
    	$title = htmlentities($title, ENT_NOQUOTES ,'UTF-8');
    	$title = preg_replace('`&([a-z]{1,2})(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig);`i','$1',$title);
    	$title = preg_replace(array('`[^a-z0-9]`i','`[-]+`'),'-',$title);
        if($this->searchPost($title)) $title = $title.'-'.time();
    	return strtolower(trim($title,'-'));
    }
}

class post{
    private $id;
    private $title;
    private $date;
    private $content;
    
    public function __construct($id = '', $title = '', $date = '', $content = ''){
        if($id != ''){
            $this->id = $id;
            $this->title = $title;
            $this->date = $date;
            $this->content = $content;
        }
    }
    
    public function get($k){
        return $this->$k;
    }
    
    public function set($k, $v){
        $this->$k = trim($v);
    }
}

class comment{
    private $id;
    private $author;
    private $date;
    private $content;
    private $parent;
    
    public function __construct($id = '', $author = '', $date = '', $content = '', $parent = ''){
        if($id != ''){
            $this->id = $id;
            $this->author = $author;
            $this->date = $date;
            $this->content = $content;
            $this->parent = $parent;
        }
    }
    
    public function get($k){
        return $this->$k;
    }
    
    public function set($k, $v){
        $this->$k = trim(strip_tags($v));
    }
}

class parser{
    private $Parsedown;
    
    public function __construct(){
        $this->Parsedown = new Parsedown();
    }
    
    public function posts($posts, $nextStart, $logged, $more, $token){
        $html = '';
        $model = file_get_contents('template/default/posts.html');
        foreach($posts as $k=>$post){
            $temp = str_replace(array('[id]', '[title]', '[date]', '[content]', '[nbComments]', '[token]'), array($post['id'], $post['title'], $post['date'], $this->Parsedown->text($post['content']), $post['nbComments'], $token), $model);
            $html.= $temp;
        }
        $scripts = ($logged) ? "showAdmin();" : "";
        if($more) $scripts.= "showMore();";
        $this->render($html, NAME, NAME, DESCRIPTION, $nextStart, $scripts, $token);
    }
    
    public function post($post, $comments, $logged, $token){
        $model = file_get_contents('template/default/post.html');
        $temp = str_replace(array('[id]', '[title]', '[date]', '[content]', '[nbComments]', '[token]'), array($post['id'], $post['title'], $post['date'], $this->Parsedown->text($post['content']), $post['nbComments'], $token), $model);
        $html = '';
        $i = 1;
        $nextStart = 0;
        $model2 = file_get_contents('template/default/comments.html');
        foreach($comments as $k=>$comment){
            $temp2 = str_replace(array('[id]', '[author]', '[date]', '[content]', '[parent]', '[token]'), array($comment['id'], $comment['author'], $comment['date'], nl2br($comment['content']), $post['id'], $token), $model2);
            $html.= $temp2;
        }
        $html = str_replace('[comments]', $html, $temp);
        $scripts = ($logged) ? "showAdmin();" : "";
        $this->render($html, NAME, $post['title'], '', '', $scripts, $token);
    }
    
    public function editPost($post, $logged){
        $model = file_get_contents('template/default/edit.html');
        $html = str_replace(array('[id]', '[title]', '[date]', '[content]'), array($post['id'], $post['title'], $post['date'], $post['content']), $model);
        $this->render($html, NAME, 'Admin', '', '', '', '');
    }
    
    public function msg($msg, $type, $back){
        $model = file_get_contents('template/default/msg.html');
        $html = str_replace(array('[msg]', '[type]', '[back]'), array($msg, $type, $back), $model);
        $this->render($html, NAME, $type, '', '', '', '');
    }
    
    private function render($content, $name, $title, $description, $nextStart, $scripts, $token){
        $model = file_get_contents('template/default/index.html');
        $html = str_replace(array('[content]', '[name]', '[title]', '[description]', '[nextStart]', '[scripts]', '[version]', '[token]'), array($content, $name, $title, $description, $nextStart, $scripts, VERSION, $token), $model);
        echo preg_replace('#<!--.*?-->#s', '', $html);
    }
}

class controller{
    private $motor;
    private $parser;
    
    public function __construct(){
        $this->motor = new motor();
        $this->parser = new parser();
        if($this->motor->get('action') == 'listPosts') $this->listPosts();
        elseif($this->motor->get('action') == 'readPost') $this->readPost();
        elseif($this->motor->get('action') == 'addComment') $this->addComment();
        elseif($this->motor->get('action') == 'install') $this->install();
        elseif($this->motor->get('action') == 'editPost') $this->editPost();
        elseif($this->motor->get('action') == 'savePost') $this->savePost();
        elseif($this->motor->get('action') == 'login') $this->login();
        elseif($this->motor->get('action') == 'delComment') $this->delComment();
        elseif($this->motor->get('action') == 'delPost') $this->delPost();
        elseif($this->motor->get('action') == 'logout') $this->logout();
    }
    
    public function listPosts(){
        $posts = array();
        $start = (isset($_GET['start'])) ? $_GET['start'] : 0;
        $i = 1;
        $nextStart = 0;
        foreach($this->motor->get('posts') as $k=>$obj) if($k >= $start){
            if($i <= BYPAGE){
                $posts[$k]['id'] = $obj->get('id');
                $posts[$k]['title'] = $obj->get('title');
                $posts[$k]['date'] = $obj->get('date');
                $posts[$k]['content'] = $obj->get('content');
                $posts[$k]['nbComments'] = $this->motor->countComments($obj->get('id'));
                $i++;
                $nextStart++;
            }
        }
        $more = ($nextStart < $this->motor->countPosts()) ? true : false;
        $this->parser->posts($posts, $nextStart, $this->motor->get('logged'), $more, $this->motor->get('token'));
    }
    
    public function readPost(){
        $obj = $this->motor->searchPost($_GET['post']);
        $post['id'] = $obj->get('id');
        $post['title'] = $obj->get('title');
        $post['date'] = $obj->get('date');
        $post['content'] = $obj->get('content');
        $post['nbComments'] = $this->motor->countComments($obj->get('id'));
        $comments = array();
        foreach($this->motor->readComments($_GET['post']) as $k=>$obj){
            $comments[$k]['id'] = $obj->get('id');
            $comments[$k]['author'] = $obj->get('author');
            $comments[$k]['date'] = $obj->get('date');
            $comments[$k]['content'] = $obj->get('content');
        }
        $this->parser->post($post, $comments, $this->motor->get('logged'), $this->motor->get('token'));
        
    }
    
    public function addComment(){
        if($this->motor->searchPost($_GET['post']) && $this->motor->checkCaptcha($_POST['author'], $_POST['captcha'], 1)){
            $comment = new comment();
            $comment->set('author', $_POST['author']);
            $comment->set('content', $_POST['content']);
            $comment->set('parent', $_GET['post']);
            if($this->motor->saveComment($comment)){
                header('location:./?post='.$_GET['post'].'#comments');
                die();
            }
        }
    }
    
    public function install(){
        if($this->motor->install()) $this->parser->msg('phpPost is installed !', 'success', './');
        else $this->parser->msg('An error has occurred', 'error', './');
    }
    
    public function editPost(){
        if($_GET['edit'] != ''){
            $obj = $this->motor->searchPost($_GET['edit']);
            $post['id'] = $obj->get('id');
            $post['title'] = $obj->get('title');
            $post['date'] = $obj->get('date');
            $post['content'] = $obj->get('content');
        }
        else{
            $post['id'] = "";
            $post['title'] = "";
            $post['date'] = date('Y-m-d H:i:s');
            $post['content'] = '';
        }
        $this->parser->editPost($post, $this->motor->get('logged'));
    }
    
    public function savePost(){
        $post = new post();
        if($_POST['id'] != ''){
            $post->set('id', $_POST['id']);
            $redirect = './?post='.$_POST['id'];
        }
        else $redirect = './';
        $post->set('title', $_POST['title']);
        $post->set('date', $_POST['date']);
        $post->set('content', $_POST['content']);
        if($this->motor->savePost($post)) $this->parser->msg('The changes have been saved', 'success', $redirect);
        else $this->parser->msg('An error has occurred', 'error', $redirect);
    }
    
    public function login(){
        if($this->motor->login($_POST['password'])){
            header('location:./');
            die();
        }
        else $this->parser->msg('Wrong password', 'error', './');
    }
    
    public function logout(){
        $this->motor->logout();
        header('location:./');
        die();
    }
    
    public function delComment(){
        if($this->motor->delComment($_GET['parent'], $_GET['del'])){
            header('location:./?post='.$_GET['parent']);
            die();
        }
    }
    
    public function delPost(){
        if($this->motor->delPost($_POST['id'])){
            header('location:./');
            die();
        }
    }
}
?>