# phpPost
A simple and lightweight PHP blogging script, using flat file database and mardown syntax.

* Use mardown to write your posts
* Moderate the comments easily
* Back office and front office together and responsive
* phpPost is extremely fast

## Installation
* Upload files to your server
* Open and complete config.php
* Visit your website
* That's all !
* Default admin password is "admin"

## Changelog

### Version 0.3
* Template changes => ok
* Adding a captcha => ok
* Adding messages (error / success) handling + dedicated template

### Version 0.2
* The crush of a post is no longer possible
* Template changes
* The button "More posts" is no longer displayed permanently
* Core optimisation
* Improving security
* Adding upload folder
* Adding a logout function

### Version 0.1
* Initial version
